$(function() {
	$.ajax({ 
		url: "https://cors-anywhere.herokuapp.com/https://docs.google.com/spreadsheets/d/e/2PACX-1vS0tIVPQOd9yThGsCeayFKeuO3tSj-ltqszbO5noNpU3zRrV0krd3dxlhlLtAs64i-TnZLb29xkY69I/pub?gid=1230705898&single=true&output=csv",
		success: function(data) {
			name_list = $.csv.toObjects(data);
			x = 0;
			name_list.forEach(function() {
				if (name_list[x]["Live"] == 0) return;

				$(".letter").append(`<p class="nameListItem"><strong>` + name_list[x]["Your Name"] + `,</strong> ` + name_list[x]["Your Role"] + `</p>`);
				x = x + 1;
			});
		}
	});
	sr.sync();
});
